/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deitel.welcomesoap;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
 
@WebService(
        name = "WelcomeSOAP",
        serviceName = "WelcomeSOAPService"
)
public class WelcomeSOAP {

    @WebMethod(operationName = "SendAccountCreationDet")

    public int SendAccountCreationDet(@WebParam(name = "accRequestDetails") accRequestDetails accRequestDetails) {
       
        return 123;

    }

    /**
     * Web service operation
     *
     * @param trnDetails
     * @return
     */
    @WebMethod(operationName = "doTransaction")
    public myResponse doTransaction(@WebParam(name = "trnDetails") trnDetails trnDetails) {
        myResponse resp = new myResponse();
        double randNum = Math.random();
        double roll = (int) Math.rint(randNum);
        String respCode = "";
        String respMsg = "";
        switch (trnDetails.getTrnType()) {
            case "Deposit":
                
                respCode = (roll == 1.0) ? "00": "96";
                respMsg = (roll == 1.0) ? "DEPOSIT OF NGN" + trnDetails.getTrnAmount() + " COMPLETE" :"BANKING HOST UNAVAILABLE";
                
                break;
            case "Withdrawal":
                respCode = (roll == 1.0) ? "00": "96";
                respMsg =  (roll == 1.0) ? "WITHDRAWAL OF NGN" + trnDetails.getTrnAmount() + " COMPLETE" :"INSUFFICIENT FUNDS";
                break;
            case "BALENQ":
                respCode = (roll == 1.0) ? "00": "96";
                respMsg =  (roll == 1.0) ? "ACCBAL NGN" + Math.round(randNum * 10000.00d)  :"SYSTEM UNAVAILABLE";
                break;

        }
        
        resp.setResponseCode(respCode);
        resp.setResponseMessage(respMsg);

        return resp;

    }

}
