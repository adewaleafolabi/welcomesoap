/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deitel.welcomesoap;

/**
 *
 * @author adewale
 */
public class accRequestDetails {

    String accname;
    double accprefintrate;
    String accshortname;
    String acctcurrency;
    String acctype;
    String branch;
    String finnaclecifid;
    String freecode10;
    String freecode3;
    String freetext12;
    String industrytype;
    String natureofadvance;
    String occucode;
    String productcode;
    String purposeofadvance;
    String relstaffid;
    int reltostaffflg;
    double sanctionamt;
    String sanctionauthority;
    String sanctiondate;
    String sanctionexpirydate;
    String sanctionlevel;
    String sanctionrefno;
    String sbucode;
    String sectorcode;
    String subsectorcode;

    public String getAccname() {
        return accname;
    }

    public void setAccname(String accname) {
        this.accname = accname;
    }

    public double getAccprefintrate() {
        return accprefintrate;
    }

    public void setAccprefintrate(double accprefintrate) {
        this.accprefintrate = accprefintrate;
    }

    public String getAccshortname() {
        return accshortname;
    }

    public void setAccshortname(String accshortname) {
        this.accshortname = accshortname;
    }

    public String getAcctcurrency() {
        return acctcurrency;
    }

    public void setAcctcurrency(String acctcurrency) {
        this.acctcurrency = acctcurrency;
    }

    public String getAcctype() {
        return acctype;
    }

    public void setAcctype(String acctype) {
        this.acctype = acctype;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getFinnaclecifid() {
        return finnaclecifid;
    }

    public void setFinnaclecifid(String finnaclecifid) {
        this.finnaclecifid = finnaclecifid;
    }

    public String getFreecode10() {
        return freecode10;
    }

    public void setFreecode10(String freecode10) {
        this.freecode10 = freecode10;
    }

    public String getFreecode3() {
        return freecode3;
    }

    public void setFreecode3(String freecode3) {
        this.freecode3 = freecode3;
    }

    public String getFreetext12() {
        return freetext12;
    }

    public void setFreetext12(String freetext12) {
        this.freetext12 = freetext12;
    }

    public String getIndustrytype() {
        return industrytype;
    }

    public void setIndustrytype(String industrytype) {
        this.industrytype = industrytype;
    }

    public String getNatureofadvance() {
        return natureofadvance;
    }

    public void setNatureofadvance(String natureofadvance) {
        this.natureofadvance = natureofadvance;
    }

    public String getOccucode() {
        return occucode;
    }

    public void setOccucode(String occucode) {
        this.occucode = occucode;
    }

    public String getProductcode() {
        return productcode;
    }

    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    public String getPurposeofadvance() {
        return purposeofadvance;
    }

    public void setPurposeofadvance(String purposeofadvance) {
        this.purposeofadvance = purposeofadvance;
    }

    public String getRelstaffid() {
        return relstaffid;
    }

    public void setRelstaffid(String relstaffid) {
        this.relstaffid = relstaffid;
    }

    public int getReltostaffflg() {
        return reltostaffflg;
    }

    public void setReltostaffflg(int reltostaffflg) {
        this.reltostaffflg = reltostaffflg;
    }

    public double getSanctionamt() {
        return sanctionamt;
    }

    public void setSanctionamt(double sanctionamt) {
        this.sanctionamt = sanctionamt;
    }

    public String getSanctionauthority() {
        return sanctionauthority;
    }

    public void setSanctionauthority(String sanctionauthority) {
        this.sanctionauthority = sanctionauthority;
    }

    public String getSanctiondate() {
        return sanctiondate;
    }

    public void setSanctiondate(String sanctiondate) {
        this.sanctiondate = sanctiondate;
    }

    public String getSanctionexpirydate() {
        return sanctionexpirydate;
    }

    public void setSanctionexpirydate(String sanctionexpirydate) {
        this.sanctionexpirydate = sanctionexpirydate;
    }

    public String getSanctionlevel() {
        return sanctionlevel;
    }

    public void setSanctionlevel(String sanctionlevel) {
        this.sanctionlevel = sanctionlevel;
    }

    public String getSanctionrefno() {
        return sanctionrefno;
    }

    public void setSanctionrefno(String sanctionrefno) {
        this.sanctionrefno = sanctionrefno;
    }

    public String getSbucode() {
        return sbucode;
    }

    public void setSbucode(String sbucode) {
        this.sbucode = sbucode;
    }

    public String getSectorcode() {
        return sectorcode;
    }

    public void setSectorcode(String sectorcode) {
        this.sectorcode = sectorcode;
    }

    public String getSubsectorcode() {
        return subsectorcode;
    }

    public void setSubsectorcode(String subsectorcode) {
        this.subsectorcode = subsectorcode;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }
    String uniqueCode;
}
