/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.deitel.welcomesoap;

/**
 *
 * @author adewale
 */
public class trnDetails {
    private double trnAmount;
    private String customerAccount;
    private String uniqueCode;
    private String trnType;

    public double getTrnAmount() {
        return trnAmount;
    }

    public void setTrnAmount(double trnAmount) {
        this.trnAmount = trnAmount;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public String getTrnType() {
        return trnType;
    }

    public void setTrnType(String trnType) {
        this.trnType = trnType;
    }
}
